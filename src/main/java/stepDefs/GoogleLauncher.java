package stepDefs;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import io.github.bonigarcia.wdm.WebDriverManager;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class GoogleLauncher {
	public WebDriver driver;// = new ChromeDriver();
	@Given("I want to write a step with precondition")
	public void i_want_to_write_a_step_with_precondition() {
		//System.setProperty("webdriver.chrome.driver", "path");
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
	    //driver.navigate().to("https://www.google.com");
		driver.get("https://www.google.com");
	}

	@Then("I validate the outcomes")
	public void i_validate_the_outcomes() {
		driver.quit();
	}
		
}
