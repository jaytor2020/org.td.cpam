package runner;
import java.io.*;

import org.junit.AfterClass;
import org.junit.runner.RunWith;
//import com.cucumber.listener.Reporter;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import managers.ConfigFileReader;

@RunWith(Cucumber.class)
@CucumberOptions(features="src/main/resources/featureFiles/", glue = "stepDefs/", 
plugin = { "pretty", "html:reports" },
//plugin={"com.aventstack.extentreports.cucumber.adapter.ExtentCucumberAdapter:" }
monochrome=true
)
public class TestRunner {
	 
}